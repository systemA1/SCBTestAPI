package com.DataModel;

public class AddressModel {
    public String getAddressNo() {
        return AddressNo;
    }

    public void setAddressNo(String addressNo) {
        AddressNo = addressNo;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getAaa() {
        return aaa;
    }

    public void setAaa(String aaa) {
        this.aaa = aaa;
    }

    private String AddressNo;
    private String Province;
    private String ZipCode;
    private String aaa;

}

