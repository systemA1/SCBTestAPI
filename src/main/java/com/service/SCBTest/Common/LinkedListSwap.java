package com.service.SCBTest.Common;

public class LinkedListSwap {
    Node head;

    /* Linked list Node*/
    class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    public void pairWiseSwap() {
        Node temp = head;

        /* Traverse only till there are atleast 2 nodes left */
        while (temp != null && temp.next != null) {

            /* Swap the data */
            int k = temp.data;
            temp.data = temp.next.data;
            temp.next.data = k;
            temp = temp.next.next;
        }
    }

    /* Utility functions */

    /* Inserts a new Node at front of the list. */
    public void push(int new_data) {
        /* 1 & 2: Allocate the Node &
                  Put in the data*/
        Node new_node = new Node(new_data);

        /* 3. Make next of new Node as head */
        new_node.next = head;

        /* 4. Move the head to point to new Node */
        head = new_node;
    }

    /* Function to print linked list */
    public String printList() {
        StringBuilder Result = new StringBuilder();
        Node temp = head;
        while (temp != null) {
            Result.append(temp.data + " ");
            //System.out.print(temp.data + " ");
            temp = temp.next;
        }
        return Result.toString();
    }

}

