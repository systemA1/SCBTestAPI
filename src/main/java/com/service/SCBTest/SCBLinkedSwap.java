package com.service.SCBTest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.service.SCBTest.Common.LinkedListSwap;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
public class SCBLinkedSwap {

    @RequestMapping("/SCBTest")
    public String SCBTest(@RequestParam String linklist, HttpServletRequest request) {

        // DecodingBase64
        String query = request.getQueryString();
        byte[] decodedBytes = Base64.getDecoder().decode(query.replace("linklist=",""));
        String decodedString = new String(decodedBytes);

        LinkedListSwap LLS = new LinkedListSwap();

        String str[] = decodedString.split("->");
        List<String> al = new ArrayList<String>();
        al = Arrays.asList(str);
        Collections.reverse(al);
        for(String s: al){
            // Push Data in LinkedList
            LLS.push(Integer.parseInt(s));
        }

        String InLink = LLS.printList();

        // Precess Swap LinkedList
        LLS.pairWiseSwap();

        return "Input : " + InLink + " | Output : " + LLS.printList();
    }

}
