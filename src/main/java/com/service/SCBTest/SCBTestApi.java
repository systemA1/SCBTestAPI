package com.service.SCBTest;

import com.service.CustomerAPI.CustomerApiApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SCBTestApi {
    public static void main(String[] args) { SpringApplication.run(SCBTestApi.class, args); }
}
