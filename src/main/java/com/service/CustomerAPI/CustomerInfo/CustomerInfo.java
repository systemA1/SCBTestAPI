package com.service.CustomerAPI.CustomerInfo;

import com.BusinessLogic.CustomerInfoBL;
import com.DataModel.CustomerModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CustomerInfo {

    @RequestMapping("/customers")
    public CustomerInfoBL GetCustomer(){

        CustomerModel c = new CustomerModel();
        c.setName("system");
        c.setSurname("Ai");
        c.setAge(32);
        c.setSurname("M");

        List<CustomerModel> customerModelList = new ArrayList<CustomerModel>();
        customerModelList.add(c);

        CustomerInfoBL customerBL = new CustomerInfoBL();
        customerBL.setCustomerModelList(customerModelList);
        return customerBL;
    }

}
