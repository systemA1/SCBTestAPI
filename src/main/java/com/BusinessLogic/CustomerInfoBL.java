package com.BusinessLogic;

import com.DataModel.CustomerModel;

import java.util.List;

public class CustomerInfoBL {
    List<CustomerModel> customerModelList;

    public List<CustomerModel> getCustomerModelList() {
        return customerModelList;
    }

    public void setCustomerModelList(List<CustomerModel> customerModelList) {
        this.customerModelList = customerModelList;
    }
}
